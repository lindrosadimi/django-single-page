1- Mise en place des dépendances
    Cette étape consiste à créer un environnement de travail adéquat pour notre projet. Il faut notifier l'image sur laquelle nous nous baserons pour nos différentes installations :
    default:
         image: python:3.9

Ensuite, les différentes commandes qui suivent seront exécutées:
    - python -V (affiche la version de python pour le debugage)
    - python -m venv venv (crée un environnement virtuel "venv")
    - source venv/bin/activate (activation de l'environnement)
    - pip install --upgrade pip (mise à jour de l'utilitaire pip)
    - pip install Django (installation de django)
    - pip install -r requirements.txt 

2- Tests
    Comme l'indque son nom, c'est l'étape des tests. Chaque fois que des modifications sont effectuées dans le code, cette étape se déclanche pour s'assurer que notre site fonctionne correctement. 
    - python3 manage.py test

3- Build
    C'est l'étape de la construction de notre code.
    - python3 manage.py makemigrations (vérifie les modifications et si il y a de nouvelles données)
    - python3 manage.py migrate (exécute les migrations détectées lors de la commande précédente)
    - python3 manage.py check

4- Deploy
    La dernière étape de notre pipeline qui consiste à enfin déployer notre site.

    (Voir Schema)
