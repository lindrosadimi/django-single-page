from django.test import TestCase
from django.urls import reverse
from django.http import Http404
from .views import texts

class SimplePageTestCase(TestCase):
    def test_index_page(self):
        # Utilise la méthode client pour simuler une requête GET sur l'URL de la page d'index.
        response = self.client.get(reverse('index'))
        # Vérifie que la requête est bien réussie, donc le code de statut doit être 200.
        self.assertEqual(response.status_code, 200)
        # Vérifie que le template utilisé est le bon
        self.assertTemplateUsed(response, 'singlepage/index.html')

# Create your tests here.
